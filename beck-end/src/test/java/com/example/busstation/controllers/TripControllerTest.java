package com.example.busstation.controllers;


import com.example.busstation.api.car.CarGetRequest;
import com.example.busstation.api.driver.DriverGetRequest;
import com.example.busstation.api.trip.TripAddRequest;
import com.example.busstation.api.trip.TripGetRequest;
import com.example.busstation.car.CarService;
import com.example.busstation.car.StatusEnum;
import com.example.busstation.driver.DriverService;
import com.example.busstation.trip.TripService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest
public class TripControllerTest {


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private TripController tripController;

    @MockBean
    private TripService tripService;

    @MockBean
    private CarService carService;

    @MockBean
    private DriverService driverService;


    @Before
    public void init() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void getAll() throws Exception {

        CarGetRequest car1 = new CarGetRequest(1L,"Shaolin",65, StatusEnum.OK);

        List<CarGetRequest> carsDriver1 = Arrays.asList(car1);

        DriverGetRequest driver1 = new DriverGetRequest(1L,"Carmen Youriyovich",55,carsDriver1);

        List<TripGetRequest> trips = Arrays.asList(
                new TripGetRequest("Kyiv-boryspil",
                        "13:20","14:40","3:00",
                        230,car1.getBrand(),driver1.getName()));

        when(tripService.findAllTrips()).thenReturn(trips);
        mvc.perform(get("/trips")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].tripName")
                        .value("Kyiv-boryspil"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].timeStart")
                        .value("13:20"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].timeEnd")
                        .value("14:40"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].tripTime")
                        .value("3:00"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].cost")
                        .value(230))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].driverName")
                        .value("Carmen Youriyovich"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].carName")
                        .value("Shaolin"));
    }

    @Test
    public void getOne() throws Exception {

        CarGetRequest car1 = new CarGetRequest(1L,"Shaolin",65, StatusEnum.OK);

        List<CarGetRequest> carsDriver1 = Arrays.asList(car1);

        DriverGetRequest driver1 = new DriverGetRequest(1L,"Carmen Youriyovich",55,carsDriver1);

        TripGetRequest trip = new TripGetRequest("Kyiv-boryspil",
                "13:20","14:40","3:00",
                230,car1.getBrand(),driver1.getName());

        when(tripService.findTripById(1L)).thenReturn(trip);
        mvc.perform(get("/trip/{id}",1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.tripName")
                        .value("Kyiv-boryspil"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.timeStart")
                        .value("13:20"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.timeEnd")
                        .value("14:40"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.tripTime")
                        .value("3:00"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cost")
                        .value(230))
                .andExpect(MockMvcResultMatchers.jsonPath("$.driverName")
                        .value("Carmen Youriyovich"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.carName")
                        .value("Shaolin"));
    }

    @Test
    public void newTrip() throws Exception {

        TripAddRequest newTrip = new TripAddRequest("Otkyda - Kyda", "14:45",
                "15:34", 25, 1L, 1L);

        Gson gson = new Gson();
        String json = gson.toJson(newTrip);

        mvc.perform(post("/trip")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists());
    }

    @Test
    public void updateTrip() throws Exception {
        TripAddRequest newTrip = new TripAddRequest("Otkyda - Kyda", "14:45",
                "15:34", 25, 1L, 1L);

        Gson gson = new Gson();
        String json = gson.toJson(newTrip);


        mvc.perform(MockMvcRequestBuilders.put("/trip/update/{id}",1)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteOne() throws Exception {

        mvc.perform(MockMvcRequestBuilders.delete("/trip/delete/{id}",1L))
                .andExpect(status().isAccepted());
    }
}