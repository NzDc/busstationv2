package com.example.busstation.controllers;

import com.example.busstation.api.car.CarGetRequest;
import com.example.busstation.api.driver.DriverAddRequest;
import com.example.busstation.api.driver.DriverGetRequest;
import com.example.busstation.car.CarService;
import com.example.busstation.car.StatusEnum;
import com.example.busstation.driver.DriverService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class DriverControllerTest {


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private DriverController driverController;

    @MockBean
    private DriverService driverService;

    @MockBean
    private CarService carService;

    @MockBean
    private TripController tripController;


    @Before
    public void init() throws Exception{
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }


    @Test
    public void getAll() throws Exception{

        CarGetRequest car1 = new CarGetRequest(1L,"Car1",100, StatusEnum.OK);
        CarGetRequest car2 = new CarGetRequest(2L,"Car2",100, StatusEnum.DEFECTIVE);

        List<CarGetRequest> carsDriver1 = Arrays.asList(car1);
        List<CarGetRequest> carsDriver2 = Arrays.asList(car2);

        List<DriverGetRequest> drivers = Arrays.asList(
                new DriverGetRequest(1L,"Car Driver1",35,carsDriver1),
                new DriverGetRequest(2L,"Car Driver2",45,carsDriver2));

        when(driverService.findAllDrivers()).thenReturn(drivers);
        mvc.perform(get("/drivers")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Car Driver1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(35))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].cars").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value("Car Driver2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(45))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].cars").isNotEmpty());
    }

    @Test
    public void getOneById() throws Exception{

        CarGetRequest car = new CarGetRequest(1L,"Kek",100,StatusEnum.OK);

        List<CarGetRequest> cars = Arrays.asList(car);

        when(driverService.findDriverById(1L)).thenReturn(new DriverGetRequest(
                1L,"Car Driver1",65,cars));

        mvc.perform(get("/driver/{id}",1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("id").value(1));
    }

    @Test
    public void newDriver() throws Exception {


        DriverAddRequest driverAddRequest = new DriverAddRequest("Car Driver1",45,1L);

        System.out.println(asJsonString(driverAddRequest));

        Gson gson = new Gson();
        String json = gson.toJson(driverAddRequest);
        System.out.println(json);

        mvc.perform(MockMvcRequestBuilders
                .post("/driver")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(json)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists());
    }

    public static String asJsonString(final Object obj){
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }   catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Test
    public void deleteOne() throws Exception{

        mvc.perform(MockMvcRequestBuilders.delete("/driver/delete/{id}",1L))
                .andExpect(status().isAccepted());
    }
}