package com.example.busstation.controllers;

import com.example.busstation.api.car.CarAddRequest;
import com.example.busstation.car.*;
import com.example.busstation.driver.DriverService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan//({"com.example.busstation.car"})
@ActiveProfiles("test")
public class CarControllerIntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;

    @Autowired
    private CarService carService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private CarRepository carRepository;

    @Before
    public void init() throws Exception{
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        CarEntity car1 = new CarEntity("Volvo",100,StatusEnum.DEFECTIVE);
        CarEntity car2 = new CarEntity("Mercedes",100, StatusEnum.OK);
        carService.addCar(car1);
        carService.addCar(car2);

    }


    @Test
    public void getAll() throws  Exception{

        mvc.perform(get("/cars")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].brand").value("Volvo"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].capability").value(100))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].status").value("DEFECTIVE"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].brand").value("Mercedes"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].capability").value(100))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].status").value("OK"));
    }

    @Test
    public void getOneById() throws Exception {

        mvc.perform(get("/car/{id}",1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("id").value(1));
    }

    @Test
    public void addCar() throws Exception {

        CarAddRequest newCar = new CarAddRequest("Volvo",55,StatusEnum.OK);
        Gson gson = new Gson();
        String json = gson.toJson(newCar);

        mvc.perform(post("/car")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brand").exists());
    }

    @Test
    public void updateCar() throws Exception {

        mvc.perform(put("/car/update/{id}",1))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCar() throws Exception {
        mvc.perform(delete("/car/delete/{id}",1))
                .andExpect(status().isAccepted());
    }
}