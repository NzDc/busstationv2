package com.example.busstation.controllers;

import com.example.busstation.api.driver.DriverAddRequest;
import com.example.busstation.car.CarEntity;
import com.example.busstation.car.CarService;
import com.example.busstation.car.StatusEnum;
import com.example.busstation.driver.DriverEntity;
import com.example.busstation.driver.DriverRepository;
import com.example.busstation.driver.DriverService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan
@ActiveProfiles("test")
public class DriverControllerIntegrationTest {

    private MockMvc mvc;

    @Autowired
    private DriverService driverService;

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CarService carService;
    @Before
    public void init() throws Exception{
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
        CarEntity car = new CarEntity("Volvo",100, StatusEnum.DEFECTIVE);
        CarEntity car2 = new CarEntity("Iveco",0, StatusEnum.OK);
        carService.addCar(car);
        carService.addCar(car2);
        List<CarEntity> cars = Arrays.asList(car);

        DriverEntity driver = new DriverEntity("Vadila Privet",33,cars);
        driverService.addDriver(driver);
    }

    @Test
    public void getAll() throws Exception {

        mvc.perform(get("/drivers").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Vadila Privet"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(33))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].cars").isNotEmpty());
    }

    @Test
    public void getOne() throws Exception {

        mvc.perform(get("/driver/{id}",1).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    public void newDriver() throws Exception {

        DriverAddRequest driverAddRequest = new DriverAddRequest("Master Lamaister",
                33,2L);

        Gson gson = new Gson();
        String json = gson.toJson(driverAddRequest);

        mvc.perform(post("/driver")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists());
    }

    @Test
    public void deleteOne() throws Exception {

        mvc.perform(delete("/driver/delete/{id}",1))
                .andExpect(status().isAccepted());
    }
}