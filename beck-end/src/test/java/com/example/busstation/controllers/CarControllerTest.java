package com.example.busstation.controllers;

import com.example.busstation.api.car.CarAddRequest;
import com.example.busstation.api.car.CarGetRequest;
import com.example.busstation.car.CarService;
import com.example.busstation.car.StatusEnum;
import com.example.busstation.driver.DriverService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CarControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    private CarController carController;

    @MockBean
    private CarService carService;

    @MockBean
    private DriverService driverService;

    @MockBean
    private TripController tripController;


    @Before
    public void init() throws Exception{
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }


    @Test
    public void getAll() throws Exception {

       List<CarGetRequest> cars = Arrays.asList(
               new CarGetRequest(1L,"Volvo",100,StatusEnum.DEFECTIVE),
               new CarGetRequest(2L,"Mercedes",100,StatusEnum.OK));

       when(carService.findAllCars()).thenReturn(cars);

       mvc.perform(get("/cars")
               .accept(MediaType.APPLICATION_JSON))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(1))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].brand").value("Volvo"))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].capability").value(100))
               .andExpect(MockMvcResultMatchers.jsonPath("$[0].status").value("DEFECTIVE"))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(2))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].brand").value("Mercedes"))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].capability").value(100))
               .andExpect(MockMvcResultMatchers.jsonPath("$[1].status").value("OK"));
       verify(carService,times(1)).findAllCars();
       verifyNoMoreInteractions(carService);
    }

    @Test
    public void getOneById() throws Exception {

        when(carService.findCarById(1L)).thenReturn(new CarGetRequest(
                1L,"Volvo",100,StatusEnum.DEFECTIVE));

        mvc.perform(get("/car/{id}",1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("id").value(1));
    }

    @Test
    public void createCar() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                .post("/car")
                .content(asJsonString(new CarAddRequest("Volvo",
                        100,StatusEnum.OK)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brand").exists());
    }

    public static String asJsonString(final Object obj){
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }   catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Test
    public void update() throws Exception {

        mvc.perform(MockMvcRequestBuilders.put("/car/update/{id}",2))
                /*.content("OK")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                */.andExpect(status().isOk());
    }

    @Test
    public void deleteCar() throws Exception {

        mvc.perform(MockMvcRequestBuilders.delete("/car/delete/{id}",1))
                .andExpect(status().isAccepted());
    }
}