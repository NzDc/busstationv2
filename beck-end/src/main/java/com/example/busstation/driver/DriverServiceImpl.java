package com.example.busstation.driver;

import com.example.busstation.api.driver.DriverAddRequest;
import com.example.busstation.api.driver.DriverGetRequest;
import com.example.busstation.car.CarEntity;
import com.example.busstation.car.CarRepository;
import com.example.busstation.car.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DriverServiceImpl implements DriverService {

    @Autowired
    private DriverRepository driverRepository;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarService carService;

    @Override
    public void deleteDriverById(Long id){
        driverRepository.deleteById(id);
    }

    @Override
    public DriverGetRequest addDriver(DriverAddRequest driver) {

        CarEntity car = carRepository.findById(driver.getCarId()).get();

        DriverEntity newDriver = new DriverEntity();
        newDriver.setName(driver.getName());
        newDriver.setAge(driver.getAge());

        ArrayList<CarEntity> carsList = new ArrayList<>();
        carsList.add(car);

        newDriver.setCars(carsList);
        driverRepository.save(newDriver);
        return new DriverGetRequest(newDriver);
    }

    @Override
    public DriverEntity addDriver(DriverEntity driver) {
        return driverRepository.save(driver);
    }

    @Override
    public DriverGetRequest updateDriver(DriverAddRequest driver, Long id){

        DriverEntity driverEntity = driverRepository.findById(id).get();
        driverEntity.setAge(driver.getAge());
        driverEntity.setName(driver.getName());

        driverRepository.save(driverEntity);

        return new DriverGetRequest(driverEntity);
    }

    @Override
    public DriverGetRequest findDriverById(Long id){

        return new DriverGetRequest(driverRepository.findById(id).get());
    }

    @Override
    public List<DriverGetRequest> findAllDrivers(){

        return driverRepository.findAll().stream()
                .map(DriverGetRequest::new)
                .collect(Collectors.toList());
    }

    @Override
    public DriverGetRequest addCarByIdToDriver(Long carId, Long driverId){

        CarEntity car = carRepository.findById(carId).get();

        DriverEntity driver = driverRepository.findById(driverId).get();

        List<CarEntity> cars = driver.getCars();

        cars.add(car);

        driver.setCars(cars);

        driverRepository.save(driver);

        return new DriverGetRequest(driver);
    }
}
