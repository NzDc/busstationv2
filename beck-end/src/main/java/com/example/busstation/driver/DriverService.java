package com.example.busstation.driver;

import com.example.busstation.api.driver.DriverAddRequest;
import com.example.busstation.api.driver.DriverGetRequest;

import java.util.List;

public interface DriverService {

    public void deleteDriverById(Long id);
    public DriverGetRequest addDriver(DriverAddRequest driver);
    public DriverEntity addDriver(DriverEntity driver);
    public DriverGetRequest updateDriver(DriverAddRequest driver, Long id);
    public DriverGetRequest findDriverById(Long id);
    public List<DriverGetRequest> findAllDrivers();
    public DriverGetRequest addCarByIdToDriver(Long carId, Long driverId);
}
