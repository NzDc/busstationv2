package com.example.busstation.driver;

import com.example.busstation.api.driver.DriverAddRequest;
import com.example.busstation.car.CarEntity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "Driver")
public class DriverEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int age;

    @OneToMany()
    private List<CarEntity> cars;



    public DriverEntity() {}


    public DriverEntity(DriverAddRequest driverAddRequest){
        this.name = driverAddRequest.getName();
        this.age = driverAddRequest.getAge();
    }

    public DriverEntity(String name, int age, List<CarEntity> cars){
        this.name = name;
        this.age = age;
        this.cars = cars;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<CarEntity> getCars() {
        return cars;
    }

    public void setCars(List<CarEntity> cars) {
        this.cars = cars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DriverEntity that = (DriverEntity) o;
        return age == that.age &&
                id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(cars, that.cars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
