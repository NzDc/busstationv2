package com.example.busstation.trip;

import com.example.busstation.api.trip.TripAddRequest;
import com.example.busstation.car.CarEntity;
import com.example.busstation.driver.DriverEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Trip")
public class TripEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String timeStart;
    private String timeEnd;
    private String tripTime;
    private double cost;

    @OneToOne
    private CarEntity car;

    @OneToOne
    private DriverEntity driver;


    TripEntity() {}

    TripEntity(TripAddRequest tripAdd){
        this.name = tripAdd.getName();
        this.timeStart = tripAdd.getTimeStart();
        this.timeEnd = tripAdd.getTimeEnd();
        this.tripTime = tripTimeDetermine(tripAdd.getTimeStart(),tripAdd.getTimeEnd());
        this.cost = tripAdd.getCost();
    }

    TripEntity(String name, String timeStart, String timeEnd, double cost, DriverEntity driver, CarEntity car) {
        this.name = name;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.cost = cost;
        this.driver = driver;
        this.car = car;
        this.tripTime = tripTimeDetermine(timeStart,timeEnd);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTripTime() {
        return tripTime;
    }

    public void setTripTime(String tripTime) {
        this.tripTime = tripTime;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    public CarEntity getCar() {
        return car;
    }

    public void setCar(CarEntity car) {
        this.car = car;
    }

    public static String tripTimeDetermine(String StartTime, String EndTime) {

        int hours = 0;
        int minutes = 0;
        int[] StartTPart = new int[2];
        int[] EndTPart = new int[2];
        String Time;
        String[] split1 = StartTime.split(":");

        for(int i=0; i<split1.length; i++) StartTPart[i] = Integer.parseInt(split1[i]);

        split1 = EndTime.split(":");

        for(int i=0; i<split1.length; i++) EndTPart[i] = Integer.parseInt(split1[i]);

        if(EndTPart[0]>=StartTPart[0]) {
            hours = EndTPart[0] - StartTPart[0];
        }

        else{
            hours = 24-StartTPart[0];
            hours += EndTPart[0];
        }

        if ((EndTPart[1]-StartTPart[1])<0) {
            hours--;
            minutes = 60 + (EndTPart[1]-StartTPart[1]);
            Time = Integer.toString(hours);
            Time +=":";
            Time += Integer.toString(minutes);
            return Time;
        }
        else {
            Time = Integer.toString(hours);
            minutes = (EndTPart[1]-StartTPart[1]);
            Time +=":";
            Time += Integer.toString(minutes);
            return Time;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripEntity trip = (TripEntity) o;
        return Double.compare(trip.cost, cost) == 0 &&
                id.equals(trip.id) &&
                Objects.equals(name, trip.name) &&
                Objects.equals(timeStart, trip.timeStart) &&
                Objects.equals(timeEnd, trip.timeEnd) &&
                Objects.equals(tripTime, trip.tripTime) &&
                Objects.equals(driver, trip.driver) &&
                Objects.equals(car, trip.car);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, timeStart, timeEnd, tripTime, cost, driver, car);
    }


}