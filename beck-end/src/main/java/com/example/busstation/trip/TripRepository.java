package com.example.busstation.trip;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TripRepository extends JpaRepository<TripEntity,Long> {

    @Query(value = "SELECT * FROM TRIP WHERE name = ?1", nativeQuery = true)
    List<TripEntity> findTripsByName(String name);
}
