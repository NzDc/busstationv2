package com.example.busstation.trip;

import com.example.busstation.api.trip.TripGetRequest;
import com.example.busstation.api.trip.TripAddRequest;
import com.example.busstation.car.CarEntity;
import com.example.busstation.car.CarRepository;
import com.example.busstation.car.CarService;
import com.example.busstation.car.StatusEnum;
import com.example.busstation.driver.DriverRepository;
import com.example.busstation.driver.DriverService;
import com.example.busstation.exceptions.WrongStatusException;
import com.example.busstation.exceptions.WrongVehicleOwnerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TripServiceImpl implements TripService {

    @Autowired
    private DriverRepository driverRepository;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private CarService carService;

    @Autowired
    private DriverService driverService;

    @Autowired
    private TripRepository tripRepository;

    @Override
    public void deleteTrip(Long id){
        tripRepository.deleteById(id);
    }

    @Override
    public TripGetRequest addTrip (TripAddRequest tripAdd)   {

        boolean flag = false;

        TripEntity newTrip = new TripEntity(tripAdd);

        newTrip.setCar(carRepository.findById(tripAdd.getCarId()).get());
        newTrip.setDriver(driverRepository.findById(tripAdd.getDriverId()).get());

        List<CarEntity> carsList = driverRepository.findById(tripAdd.getDriverId()).get().getCars();

        for(CarEntity carCheck : carsList) {
            if (carCheck.getId() == tripAdd.getCarId()) {
                flag = true;
            }
        }

        if (!flag) {
            throw new WrongVehicleOwnerException(tripAdd.getCarId());
        }

        if(carService.findCarById(tripAdd.getCarId())
                .getStatus() == StatusEnum.DEFECTIVE) {

            throw new WrongStatusException(tripAdd.getCarId());
        }

        tripRepository.save(newTrip);
        return new TripGetRequest(newTrip);
    }

    //String tripName,String timeStart,String timeEnd
    @Override
    public TripGetRequest updateTrip(TripAddRequest tripAddRequest, Long id){

        TripEntity tripEntity = tripRepository.findById(id).get();

        tripEntity.setName(tripAddRequest.getName());
        tripEntity.setTimeStart(tripAddRequest.getTimeStart());
        tripEntity.setTimeEnd(tripAddRequest.getTimeEnd());
        tripEntity.setTripTime(TripEntity.tripTimeDetermine(
                tripAddRequest.getTimeStart(), tripAddRequest.getTimeEnd()));
        tripEntity.setCost(tripAddRequest.getCost());

        tripRepository.save(tripEntity);
        return new TripGetRequest(tripEntity);
    }

    @Override
    public TripGetRequest findTripById(Long id){

        return new TripGetRequest(tripRepository
                    .findById(id).get()
        );
    }

    @Override
    public List<TripGetRequest> findAllTrips () {

        return tripRepository.findAll().stream()
               .map(TripGetRequest::new)
               .collect(Collectors.toList());
    }

    @Override
    public List<TripGetRequest> findTripsByDriverId(Long id) {

        return tripRepository.findAll().stream()
               .filter(trip -> trip.getDriver().getId() == id)
               .map(TripGetRequest::new)
               .collect(Collectors.toList());
    }
}
