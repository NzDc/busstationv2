package com.example.busstation.trip;


import com.example.busstation.api.trip.TripGetRequest;
import com.example.busstation.api.trip.TripAddRequest;

import java.util.List;

public interface TripService {

    public void deleteTrip(Long id);
    public TripGetRequest addTrip(TripAddRequest tripAdd);
    public TripGetRequest updateTrip(TripAddRequest tripAddRequest, Long id);
    public TripGetRequest findTripById(Long id);
    public List<TripGetRequest> findAllTrips();
    public List<TripGetRequest> findTripsByDriverId(Long id);
   // public String tripTimeDetermine(String StartTime, String EndTime);
}
