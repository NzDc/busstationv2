package com.example.busstation.controllers;


import com.example.busstation.api.car.CarAddRequest;
import com.example.busstation.api.car.CarGetRequest;
import com.example.busstation.api.driver.DriverGetRequest;
import com.example.busstation.car.CarService;
import com.example.busstation.car.StatusEnum;
import com.example.busstation.driver.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    private DriverService driverService;

    @GetMapping("/cars")
    public List<CarGetRequest> getAll(){

        return carService.findAllCars();
    }

    @GetMapping("/cars/OK")
    public List<CarGetRequest> getOK(){
        return carService.findCarsByOkStatus();
    }

    @GetMapping("/car/{id}")
    public ResponseEntity<CarGetRequest> getOne (@PathVariable Long id){
        return new ResponseEntity<CarGetRequest>(carService.findCarById(id),HttpStatus.OK);
    }

    @PostMapping("/car")
    public ResponseEntity<CarAddRequest> newCar (@RequestBody CarAddRequest newCar) {

        carService.addCar(newCar);
        return new ResponseEntity<CarAddRequest>(newCar,HttpStatus.CREATED);
    }

    @PostMapping("/car/{id}/addTo/{idDriver}")
    public DriverGetRequest addCarToDriver(@PathVariable Long id, @PathVariable Long idDriver){
        return driverService.addCarByIdToDriver(id,idDriver);
    }

    @PutMapping("/car/update/{id}")
    public ResponseEntity<String> updateCar (@PathVariable Long id){

        carService.updateCarStatus(id);

        return new ResponseEntity<>("Status has been changed",HttpStatus.OK);
    }

    @DeleteMapping("/car/delete/{id}")
    public ResponseEntity<HttpStatus> deleteOne (@PathVariable Long id){
        carService.deleteCarById(id);

        return new ResponseEntity<HttpStatus>(HttpStatus.ACCEPTED);
    }
}
