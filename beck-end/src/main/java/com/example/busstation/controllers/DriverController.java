package com.example.busstation.controllers;


import com.example.busstation.api.driver.DriverGetRequest;
import com.example.busstation.car.CarService;
import com.example.busstation.api.driver.DriverAddRequest;
import com.example.busstation.driver.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DriverController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private CarService carService;

    @GetMapping("/drivers")
    public List<DriverGetRequest> getAll(){

        return driverService.findAllDrivers();
    }

    @GetMapping("/driver/{id}")
    public DriverGetRequest getOne (@PathVariable Long id){
        return driverService.findDriverById(id);
    }

    @PostMapping("/driver")
    public ResponseEntity<DriverAddRequest> newDriver ( @RequestBody DriverAddRequest driver) {

        System.out.println(driver);
        driverService.addDriver(driver);
        return new ResponseEntity<DriverAddRequest>(driver, HttpStatus.CREATED);
    }


    @PutMapping("/driver/{id}")
    private DriverGetRequest updateDriver (@RequestBody DriverAddRequest update, @PathVariable Long id){
        return driverService.updateDriver(update,id);
    }

    @DeleteMapping("/driver/delete/{id}")
    public ResponseEntity<HttpStatus> deleteOne(@PathVariable Long id) {

        driverService.deleteDriverById(id);
        return new ResponseEntity<HttpStatus> (HttpStatus.ACCEPTED);
    }
}
