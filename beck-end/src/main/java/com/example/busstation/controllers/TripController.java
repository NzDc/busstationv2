package com.example.busstation.controllers;

import com.example.busstation.api.trip.TripAddRequest;
import com.example.busstation.api.trip.TripGetRequest;
import com.example.busstation.trip.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TripController {

    @Autowired
    private TripService tripService;


    @GetMapping("/trips")
    public List<TripGetRequest> getAll(){

        return tripService.findAllTrips();
    }


    @GetMapping("/trip/{id}")
    public TripGetRequest getOne (@PathVariable Long id){
        return tripService.findTripById(id);
    }

    @PostMapping("/trip")
    public ResponseEntity<TripAddRequest> newTrip (@RequestBody TripAddRequest newTrip) {

        tripService.addTrip(newTrip);
        return new ResponseEntity<TripAddRequest>(newTrip,HttpStatus.CREATED);
    }

    //String tripName, String timeStart, String timeEnd, double cost
    @PutMapping("/trip/update/{id}")
    public ResponseEntity <TripGetRequest> updateTrip (@PathVariable Long id,
                                                       @RequestBody TripAddRequest updatedTrip) {

        return new ResponseEntity<TripGetRequest>(
                tripService.updateTrip(updatedTrip, id), HttpStatus.OK);
    }

    @DeleteMapping("/trip/delete/{id}")
    public ResponseEntity<HttpStatus> deleteOne(@PathVariable Long id) {

        tripService.deleteTrip(id);
        return new ResponseEntity<HttpStatus>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/trip/driver/{id}")
    List<TripGetRequest> getTripForDriver(@PathVariable Long id){
        return tripService.findTripsByDriverId(id);
    }
}
