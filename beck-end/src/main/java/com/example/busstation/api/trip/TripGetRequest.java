package com.example.busstation.api.trip;

import com.example.busstation.trip.TripEntity;

public class TripGetRequest {

    private String tripName;
    private String timeStart;
    private String timeEnd;
    private String tripTime;
    private double cost;
    private String carName;
    private String driverName;

    public TripGetRequest(){}

    public TripGetRequest(String tripName, String timeStart, String timeEnd,
                          String tripTime, double cost, String carName,
                          String driverName) {
        this.tripName = tripName;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.tripTime = tripTime;
        this.cost = cost;
        this.carName = carName;
        this.driverName = driverName;
    }

    public TripGetRequest(TripEntity tripEntity){
        this.tripName = tripEntity.getName();
        this.timeStart = tripEntity.getTimeStart();
        this.timeEnd = tripEntity.getTimeEnd();
        this.tripTime = tripEntity.getTripTime();
        this.cost = tripEntity.getCost();
        this.carName = tripEntity.getCar().getBrand();
        this.driverName = tripEntity.getDriver().getName();
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getTripTime() {
        return tripTime;
    }

    public void setTripTime(String tripTime) {
        this.tripTime = tripTime;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }
}
