package com.example.busstation.api.trip;

public class TripAddRequest {

    private String name;
    private String timeStart;
    private String timeEnd;
    private double cost;
    private Long driverId;
    private Long carId;

    public TripAddRequest(String name, String timeStart, String timeEnd, double cost, Long driverId, Long carId) {
        this.name = name;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.cost = cost;
        this.driverId = driverId;
        this.carId = carId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }


    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }
}
