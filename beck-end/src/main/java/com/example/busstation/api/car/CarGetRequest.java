package com.example.busstation.api.car;

import com.example.busstation.car.CarEntity;
import com.example.busstation.car.StatusEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class CarGetRequest {
    private Long id;
    private String brand;
    private int capability;
    private StatusEnum status;

    public CarGetRequest(){
    }

    public CarGetRequest(CarEntity carEntity){
        this.id = carEntity.getId();
        this.brand = carEntity.getBrand();
        this.capability = carEntity.getCapability();
        this.status = carEntity.getStatus();
    }

    public CarGetRequest(Long id, String brand, int capability, StatusEnum status){
        this.id = id;
        this.brand = brand;
        this.capability = capability;
        this.status = status;
    }
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCapability() {
        return capability;
    }

    public void setCapability(int capability) {
        this.capability = capability;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CarGetRequest{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", capability=" + capability +
                ", status=" + status +
                '}';
    }
}
