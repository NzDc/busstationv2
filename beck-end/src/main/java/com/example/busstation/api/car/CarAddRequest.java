package com.example.busstation.api.car;

import com.example.busstation.car.CarEntity;
import com.example.busstation.car.StatusEnum;

public class CarAddRequest {


    private String brand;
    private int capability;
    private StatusEnum status;

    public CarAddRequest(){
    }

    public CarAddRequest(String brand,int capability,StatusEnum status) {
        this.brand = brand;
        this.capability = capability;
        this.status = status;
    }

    CarAddRequest(CarEntity carEntity){
        this.brand = carEntity.getBrand();
        this.capability = carEntity.getCapability();
        this.status = carEntity.getStatus();
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCapability() {
        return capability;
    }

    public void setCapability(int capability) {
        this.capability = capability;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
}
