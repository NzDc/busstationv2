package com.example.busstation.api.driver;



public class DriverAddRequest {

    private String name;
    private Integer age;
    private Long carId;

    public DriverAddRequest(String name, int age, Long carId) {
        this.age = age;
        this.carId=carId;
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }


    @Override
    public String toString() {
        return "DriverAddRequest{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", carId=" + carId +
                '}';
    }

}
