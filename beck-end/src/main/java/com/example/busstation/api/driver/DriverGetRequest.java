package com.example.busstation.api.driver;

import com.example.busstation.api.car.CarGetRequest;
import com.example.busstation.driver.DriverEntity;

import java.util.List;
import java.util.stream.Collectors;

public class DriverGetRequest {
    private Long id;
    private String name;
    private int age;
    private List<CarGetRequest> cars;

    public DriverGetRequest(){}

    public DriverGetRequest(Long id, String name, int age, List<CarGetRequest> cars){
        this.id = id;
        this.name = name;
        this.age = age;
        this.cars = cars;
    }
    public DriverGetRequest(DriverEntity driverEntity){
        this.id = driverEntity.getId();
        this.name = driverEntity.getName();
        this.age = driverEntity.getAge();
        this.cars = driverEntity.getCars().stream()
                .map(CarGetRequest::new)
                .collect(Collectors.toList());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<CarGetRequest> getCars() {
        return cars;
    }

    public void setCars(List<CarGetRequest> cars) {
        this.cars = cars;
    }
}
