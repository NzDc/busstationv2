package com.example.busstation.exceptions;

public class WrongVehicleOwnerException extends RuntimeException {
    public WrongVehicleOwnerException(Long id){
        super("Vehicle with id: " + id + " does not belong to this driver");
    }
}
