package com.example.busstation.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class WrongVehicleOwnerAdvice {

        @ResponseBody
        @ExceptionHandler(WrongVehicleOwnerException.class)
        @ResponseStatus(HttpStatus.CONFLICT)
        String WrongVehicleOwnerHandle (WrongVehicleOwnerException ex)  {
                return ex.getMessage();
        }
}
