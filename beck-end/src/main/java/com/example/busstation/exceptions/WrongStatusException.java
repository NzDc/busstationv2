package com.example.busstation.exceptions;

public class WrongStatusException extends RuntimeException {

    public WrongStatusException(Long id){
        super("Could not add trip with vehicle id " +
                id + " . Vehicle is broken");
    }
}
