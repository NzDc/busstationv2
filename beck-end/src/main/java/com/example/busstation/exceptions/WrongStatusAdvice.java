package com.example.busstation.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class WrongStatusAdvice {

    @ResponseBody
    @ExceptionHandler(WrongStatusException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String wrongStatusHandler(WrongStatusException ex){
        return ex.getMessage();
    }
}
