package com.example.busstation.car;

import com.example.busstation.api.car.CarAddRequest;
import com.example.busstation.api.car.CarGetRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Override
    public void deleteCarById(Long id){
        carRepository.deleteById(id);
    }

    @Override
    public CarGetRequest addCar (CarAddRequest car) {

        CarEntity carEntity = new CarEntity(car);

        carRepository.save(carEntity);
        return new CarGetRequest(carEntity);
    }

    public CarEntity addCar(CarEntity car) {
        return carRepository.save(car);
    }

    @Override
    public CarGetRequest updateCarStatus (Long id) {

        CarEntity carEntity = carRepository.findById(id).get();

        if(carEntity.getStatus() == StatusEnum.OK){
            carEntity.setStatus(StatusEnum.DEFECTIVE);
        } else {
            carEntity.setStatus(StatusEnum.OK);
        }

        carRepository.save(carEntity);
        return new CarGetRequest(carEntity);
    }


    @Override
    public CarGetRequest findCarById(Long id){
        return new CarGetRequest(carRepository.findById(id).get());
    }

    @Override
    public List<CarGetRequest> findAllCars(){
        return carRepository.findAll().stream()
                .map(CarGetRequest::new).collect(Collectors.toList());
    }

    @Override
    public  List<CarGetRequest> findCarsByOkStatus(){
        return carRepository.findAll().stream()
                .filter(car -> car.getStatus() == StatusEnum.OK)
                .map(CarGetRequest::new)
                .collect(Collectors.toList());
    }
}
