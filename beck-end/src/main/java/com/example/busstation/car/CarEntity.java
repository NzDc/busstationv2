package com.example.busstation.car;

import com.example.busstation.api.car.CarAddRequest;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Car")
public class CarEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String brand;
    private int capability;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

    public CarEntity() {}

    public CarEntity(CarAddRequest carRequest){
        this.brand = carRequest.getBrand();
        this.capability = carRequest.getCapability();
        this.status = carRequest.getStatus();
    }

    public CarEntity(String brand, int capability, StatusEnum status){
        this.brand = brand;
        this.capability = capability;
        this.status = status;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCapability() {
        return capability;
    }

    public void setCapability(int capability) {
        this.capability = capability;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CarEntity{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", capability=" + capability +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarEntity car = (CarEntity) o;
        return capability == car.capability &&
                id.equals(car.id) &&
                Objects.equals(brand, car.brand) &&
                status == car.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
