package com.example.busstation.car;

import com.example.busstation.api.car.CarAddRequest;
import com.example.busstation.api.car.CarGetRequest;

import java.util.List;

public interface CarService {

        public void deleteCarById(Long id);
        public CarGetRequest addCar(CarAddRequest car);
        public CarEntity addCar(CarEntity car);
        public CarGetRequest updateCarStatus(Long id);
        public CarGetRequest findCarById(Long id);
        public List<CarGetRequest> findAllCars();
        public  List<CarGetRequest> findCarsByOkStatus();
}
